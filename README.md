# Design an autonomous car

Complete the 'Image Segmentation' part of an embedded computer vision system for autonomous vehicles.

---

## Technologies
- Python
- Tensorflow
- Keras
- FastApi
- Streamlit
- Azure Web App
- Git

## Deep Learning models 
- Unet mini
- VGG16 Unet

---

## Data Source

Input Images (gtFine) are dowloaded here: [CITYSCAPES](https://www.cityscapes-dataset.com/dataset-overview/)

---

## Clone repository and install dependancies

git clone git@github.com:ahmedaao/design-autonomous-car.git
cd design-autonomous-car
pip install -r requirements.txt
pip install . # Install modules from package src/

---

# TODO : Complete this README.md file 